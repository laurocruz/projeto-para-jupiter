#include <stdlib.h>
#include <stdio.h>

int calculaQuantidade(int* pedido, int valor) {
	int quantidade = *pedido / valor;
	*pedido %= valor;
	
	return quantidade;
}

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
        int hidrogenio, batata, helio, gravidade, carbono, gas;

        gas = calculaQuantidade(&pedido, 50);
		carbono = calculaQuantidade(&pedido, 20);
        gravidade = calculaQuantidade(&pedido, 10);
        helio = calculaQuantidade(&pedido, 5);
        batata = calculaQuantidade(&pedido, 2);
        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d batata\n", batata);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
		printf("%d carbono \n", carbono);
        printf("%d gas\n", gas);

        return 0;
    }
}



