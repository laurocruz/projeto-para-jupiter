# Caixa Eletrônico

Esse código possui implementado
um caixa eletônico.

## Licença

MIT

## Instruções para instalação

~~~
$ make
~~~

## Instruções de uso

~~~
$ ./atm <valor>
~~~
