# Caixa Eletrônico

Esse código possui implementado
um caixa eletrônico.

## Licença

GPLv3

## Instruções para instalação

~~~
$ make
~~~

## Instruções de use

~~~
$ ./atm <value>
~~~

Exemplo:
~~~
$ atm 133
3 hidrogênio
0 helio
3 gravidade
2 gás
~~~
